#
# Cookbook Name:: lvm-cookbook
# Recipe:: default
#
# Copyright (c) 2017 The Authors, All Rights Reserved.
execute 'Become rooot' do
 command 'sudo su '
end

mylvm = data_bag_item("sod","mylvm")

lvm_volume_group mylvm["vg_name"] do
 physical_volumes mylvm["vg_devices"]
 mylvm["logical_volumes"].each do |lv| 
   logical_volume lv["lv_name"] do
    size lv["lv_size"]
    filesystem lv["lv_fs_type"]
    mount_point location: lv["lv_mount_path"], options:lv["lv_mount_options"]    
   end
 end
end
